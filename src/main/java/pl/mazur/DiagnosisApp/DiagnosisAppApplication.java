package pl.mazur.DiagnosisApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DiagnosisAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiagnosisAppApplication.class, args);
	}

}
