package pl.mazur.DiagnosisApp.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.mazur.DiagnosisApp.database.entities.SymptomEntity;
import pl.mazur.DiagnosisApp.database.shared.Sex;

import java.util.Optional;

public interface SymptomRepository extends JpaRepository<SymptomEntity, String> {
    public Optional<SymptomEntity> findFirstByName(String name);

    public Optional<SymptomEntity> findFirstBySymptomId(String symptomId);
}
