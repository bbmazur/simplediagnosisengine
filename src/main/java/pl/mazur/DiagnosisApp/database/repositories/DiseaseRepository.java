package pl.mazur.DiagnosisApp.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.mazur.DiagnosisApp.database.entities.DiseaseEntity;
import pl.mazur.DiagnosisApp.database.shared.Sex;

import java.util.List;

public interface DiseaseRepository extends JpaRepository<DiseaseEntity, String> {
    List<DiseaseEntity> findAllBySexOrSex(Sex sex1, Sex bothSex);
}
