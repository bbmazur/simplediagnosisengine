package pl.mazur.DiagnosisApp.database.entities;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import pl.mazur.DiagnosisApp.database.shared.Prevalence;
import pl.mazur.DiagnosisApp.database.shared.Sex;
import pl.mazur.DiagnosisApp.server.requests.NewDiseaseRequest;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "Disease")
@Data
public class DiseaseEntity {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(updatable = false, nullable = false, name = "disease_id")
    private String diseaseId;
    private String name;
    private Sex sex;
    private Prevalence prevalence;
    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "disease_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private List<DiseaseSymptomEntity> occurringSymptoms;
}
