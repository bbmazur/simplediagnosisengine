package pl.mazur.DiagnosisApp.database.entities;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import pl.mazur.DiagnosisApp.database.shared.Sex;

import javax.persistence.*;

@Entity
@Table(name = "Symptom")
@Data
public class SymptomEntity {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(updatable = false, nullable = false)
    private String symptomId;
    private String name;
    private Sex sex;

    public SymptomEntity() { }

    public SymptomEntity(String name, Sex sex) {
        this.name = name;
        this.sex = sex;
    }
}
