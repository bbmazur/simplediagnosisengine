package pl.mazur.DiagnosisApp.database.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data
@Table(name = "Disease_Symptom")
public class DiseaseSymptomEntity {
    @JsonIgnore
    @Id
    @GeneratedValue
    private Long id;
    @JsonIgnore
    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "disease_id")
    private DiseaseEntity disease;
    private String symptomId;
    private double probability;
}
