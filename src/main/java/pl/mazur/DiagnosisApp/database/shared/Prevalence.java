package pl.mazur.DiagnosisApp.database.shared;

public enum Prevalence {
    RARE, MODERATE, LIKELY;

    public double getNumVal() {
        switch (this.name()) {
            case "RARE":
                return 0.0005;
            case "MODERATE":
                return 0.005;
            case "LIKELY":
                return 0.03;
            default:
                return 0;
        }
    }
}
