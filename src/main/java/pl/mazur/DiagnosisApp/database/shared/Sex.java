package pl.mazur.DiagnosisApp.database.shared;

public enum Sex {
    BOTH, MALE, FEMALE
}
