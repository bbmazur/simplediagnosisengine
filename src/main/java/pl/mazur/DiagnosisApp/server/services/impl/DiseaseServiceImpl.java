package pl.mazur.DiagnosisApp.server.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import pl.mazur.DiagnosisApp.database.entities.DiseaseEntity;
import pl.mazur.DiagnosisApp.database.entities.DiseaseSymptomEntity;
import pl.mazur.DiagnosisApp.database.repositories.DiseaseRepository;
import pl.mazur.DiagnosisApp.database.repositories.SymptomRepository;
import pl.mazur.DiagnosisApp.server.requests.NewDiseaseRequest;
import pl.mazur.DiagnosisApp.server.requests.NewDiseaseSymptomRequest;
import pl.mazur.DiagnosisApp.server.services.DiseaseService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DiseaseServiceImpl implements DiseaseService {

    private final DiseaseRepository diseaseRepository;

    private final SymptomRepository symptomRepository;

    @Autowired
    public DiseaseServiceImpl(DiseaseRepository diseaseRepository, SymptomRepository symptomRepository) {
        this.diseaseRepository = diseaseRepository;
        this.symptomRepository = symptomRepository;
    }

    @Override
    public List<DiseaseEntity> getAllExistingDiseases() {
        return diseaseRepository.findAll();
    }

    @Override
    public DiseaseEntity addNewDisease(NewDiseaseRequest newDiseaseRequest) {
        if (newDiseaseRequest.getOccurringSymptoms().stream().anyMatch(s -> !symptomRepository.findFirstBySymptomId(s.getSymptomId()).isPresent())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "One of given symptoms was not found in database!");
        }
        if (newDiseaseRequest.getOccurringSymptoms().stream().anyMatch(s -> s.getProbability() > 1 || s.getProbability() <= 0)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "One of symptoms probability is not in (0, 1> range!");
        }
        return createDiseaseEntityFromRequest(newDiseaseRequest);
    }

    private DiseaseEntity createDiseaseEntityFromRequest(NewDiseaseRequest newDiseaseRequest) {
        DiseaseEntity diseaseEntity = new DiseaseEntity();
        diseaseEntity.setName(newDiseaseRequest.getName());
        diseaseEntity.setPrevalence(newDiseaseRequest.getPrevalence());
        diseaseEntity.setSex(newDiseaseRequest.getSex());
        diseaseEntity.setOccurringSymptoms(newDiseaseRequest
                .getOccurringSymptoms()
                .stream()
                .map(this::createDiseaseSymptomEntityFromRequest)
                .collect(Collectors.toList())
        );
        diseaseEntity.getOccurringSymptoms().forEach(s -> s.setDisease(diseaseEntity));
        return diseaseRepository.save(diseaseEntity);
    }

    private DiseaseSymptomEntity createDiseaseSymptomEntityFromRequest(NewDiseaseSymptomRequest newDiseaseSymptomRequest) {
        DiseaseSymptomEntity diseaseSymptomEntity = new DiseaseSymptomEntity();
        diseaseSymptomEntity.setSymptomId(newDiseaseSymptomRequest.getSymptomId());
        diseaseSymptomEntity.setProbability(newDiseaseSymptomRequest.getProbability());
        return diseaseSymptomEntity;
    }
}
