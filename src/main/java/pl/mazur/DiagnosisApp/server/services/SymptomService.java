package pl.mazur.DiagnosisApp.server.services;

import pl.mazur.DiagnosisApp.database.entities.SymptomEntity;
import pl.mazur.DiagnosisApp.server.requests.NewSymptomRequest;

import java.util.List;

public interface SymptomService {
    List<SymptomEntity> getAllExistingSymptoms();

    SymptomEntity addNewSymptom(NewSymptomRequest newSymptomRequest);
}
