package pl.mazur.DiagnosisApp.server.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import pl.mazur.DiagnosisApp.database.entities.DiseaseEntity;
import pl.mazur.DiagnosisApp.database.entities.DiseaseSymptomEntity;
import pl.mazur.DiagnosisApp.database.repositories.DiseaseRepository;
import pl.mazur.DiagnosisApp.database.repositories.SymptomRepository;
import pl.mazur.DiagnosisApp.database.shared.Sex;
import pl.mazur.DiagnosisApp.server.requests.CalculateProbableDiseaseRequest;
import pl.mazur.DiagnosisApp.server.services.DiagnosisService;
import pl.mazur.DiagnosisApp.server.to.DiagnosisTo;
import pl.mazur.DiagnosisApp.server.to.DiseaseTo;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class DiagnosisServiceImpl implements DiagnosisService {

    private final DiseaseRepository diseaseRepository;

    private final SymptomRepository symptomRepository;

    @Autowired
    public DiagnosisServiceImpl(DiseaseRepository diseaseRepository, SymptomRepository symptomRepository) {
        this.diseaseRepository = diseaseRepository;
        this.symptomRepository = symptomRepository;
    }

    @Override
    public DiagnosisTo calculateDiagnosis(CalculateProbableDiseaseRequest calculateProbableDiseaseRequest) {
        if (calculateProbableDiseaseRequest.getSex() == Sex.BOTH) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Diagnosis request cannot contain BOTH sex!");
        }
        if (calculateProbableDiseaseRequest.getReportedSymptoms().stream().anyMatch(s -> !symptomRepository.findFirstBySymptomId(s).isPresent())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "One of given symptoms was not found in database!");
        }
        //Algorithm section
        List<Pair<DiseaseEntity, List<DiseaseSymptomEntity>>> relevantSymptoms = getAllRelevantSymptoms(
                getAllRelevantDiseases(calculateProbableDiseaseRequest.getSex()),
                calculateProbableDiseaseRequest.getReportedSymptoms());
        Pair<DiseaseEntity, Double> result = calculateMostProbableDisease(relevantSymptoms);
        DiseaseEntity diseaseEntityResult = result.getFirst();

        if (result.getSecond() == 0) {
            diseaseEntityResult = new DiseaseEntity();
        }
        return new DiagnosisTo(diseaseEntityResult);
    }

    @Override
    public List<DiseaseEntity> getAllRelevantDiseases(Sex requestSex) {
        return diseaseRepository.findAllBySexOrSex(requestSex, Sex.BOTH);
    }

    @Override
    public List<Pair<DiseaseEntity, List<DiseaseSymptomEntity>>> getAllRelevantSymptoms(List<DiseaseEntity> relevantDiseases, Set<String> reportedSymptoms) {
        return relevantDiseases.stream().map(d ->
                Pair.of(
                        d,
                        d.getOccurringSymptoms().stream()
                                .filter(s -> reportedSymptoms.contains(s.getSymptomId())).collect(Collectors.toList())))
                .collect(Collectors.toList());
    }

    @Override
    public Pair<DiseaseEntity, Double> calculateMostProbableDisease(List<Pair<DiseaseEntity, List<DiseaseSymptomEntity>>> relevantSymptoms) {
        return relevantSymptoms.stream().filter(d -> d.getSecond().size() != 0).map(d -> Pair.of(d.getFirst(), d.getFirst().getPrevalence().getNumVal() *
                d.getSecond().stream()
                        .map(DiseaseSymptomEntity::getProbability)
                        .reduce(Double::sum).orElse(0.0))).max(Comparator.comparing(Pair::getSecond)).orElse(Pair.of(new DiseaseEntity(), 0.0));
    }
}
