package pl.mazur.DiagnosisApp.server.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import pl.mazur.DiagnosisApp.database.entities.SymptomEntity;
import pl.mazur.DiagnosisApp.database.repositories.SymptomRepository;
import pl.mazur.DiagnosisApp.database.shared.Sex;
import pl.mazur.DiagnosisApp.server.requests.NewSymptomRequest;
import pl.mazur.DiagnosisApp.server.services.SymptomService;

import java.util.List;
import java.util.Optional;

@Service
public class SymptomServiceImpl implements SymptomService {

    private final SymptomRepository symptomRepository;

    @Autowired
    public SymptomServiceImpl(SymptomRepository symptomRepository) {
        this.symptomRepository = symptomRepository;
    }

    @Override
    public List<SymptomEntity> getAllExistingSymptoms() {
        return symptomRepository.findAll();
    }

    @Override
    public SymptomEntity addNewSymptom(NewSymptomRequest newSymptomRequest) {
        Optional<SymptomEntity> symptomEntity = symptomRepository.findFirstByName(newSymptomRequest.getName());
        if (symptomEntity.isPresent()) {
            if (symptomEntity.get().getSex() == Sex.BOTH
            || symptomEntity.get().getSex() == newSymptomRequest.getSex()) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Same symptom already found!");
            } else {
               symptomEntity.get().setSex(Sex.BOTH);
                return symptomRepository.save(symptomEntity.get());
            }
        }
        return symptomRepository.save(new SymptomEntity(newSymptomRequest.getName(), newSymptomRequest.getSex()));
    }

}
