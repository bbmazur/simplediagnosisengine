package pl.mazur.DiagnosisApp.server.services;

import pl.mazur.DiagnosisApp.database.entities.DiseaseEntity;
import pl.mazur.DiagnosisApp.server.requests.NewDiseaseRequest;

import java.util.List;

public interface DiseaseService {
    List<DiseaseEntity> getAllExistingDiseases();

    DiseaseEntity addNewDisease(NewDiseaseRequest newDiseaseRequest);
}
