package pl.mazur.DiagnosisApp.server.services;

import org.springframework.data.util.Pair;
import pl.mazur.DiagnosisApp.database.entities.DiseaseEntity;
import pl.mazur.DiagnosisApp.database.entities.DiseaseSymptomEntity;
import pl.mazur.DiagnosisApp.database.shared.Sex;
import pl.mazur.DiagnosisApp.server.requests.CalculateProbableDiseaseRequest;
import pl.mazur.DiagnosisApp.server.to.DiagnosisTo;

import java.util.List;
import java.util.Set;

public interface DiagnosisService {
    DiagnosisTo calculateDiagnosis(CalculateProbableDiseaseRequest calculateProbableDiseaseRequest);

    List<DiseaseEntity> getAllRelevantDiseases(Sex requestSex);

    List<Pair<DiseaseEntity, List<DiseaseSymptomEntity>>> getAllRelevantSymptoms(List<DiseaseEntity> relevantDiseases, Set<String> reportedSymptoms);

    Pair<DiseaseEntity, Double> calculateMostProbableDisease(List<Pair<DiseaseEntity, List<DiseaseSymptomEntity>>> relevantSymptoms);
}
