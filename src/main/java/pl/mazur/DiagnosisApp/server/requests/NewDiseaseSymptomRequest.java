package pl.mazur.DiagnosisApp.server.requests;

import lombok.Data;

@Data
public class NewDiseaseSymptomRequest {
    private String symptomId;
    private double probability;
}
