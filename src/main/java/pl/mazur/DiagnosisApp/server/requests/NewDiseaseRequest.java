package pl.mazur.DiagnosisApp.server.requests;

import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import pl.mazur.DiagnosisApp.database.shared.Prevalence;
import pl.mazur.DiagnosisApp.database.shared.Sex;

import javax.persistence.CascadeType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import java.util.List;

@Data
public class NewDiseaseRequest {
    private String name;
    private Sex sex;
    private Prevalence prevalence;
    private List<NewDiseaseSymptomRequest> occurringSymptoms;
}
