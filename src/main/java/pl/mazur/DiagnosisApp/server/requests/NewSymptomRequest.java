package pl.mazur.DiagnosisApp.server.requests;

import lombok.Data;
import pl.mazur.DiagnosisApp.database.shared.Sex;

@Data
public class NewSymptomRequest {
    private String name;
    private Sex sex;
}
