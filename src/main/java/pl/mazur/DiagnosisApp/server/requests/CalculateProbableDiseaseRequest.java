package pl.mazur.DiagnosisApp.server.requests;

import lombok.Data;
import pl.mazur.DiagnosisApp.database.shared.Sex;

import java.util.Set;

@Data
public class CalculateProbableDiseaseRequest {
    private Sex sex;
    private Set<String> reportedSymptoms;
}
