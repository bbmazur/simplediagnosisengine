package pl.mazur.DiagnosisApp.server.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import pl.mazur.DiagnosisApp.database.entities.SymptomEntity;
import pl.mazur.DiagnosisApp.server.requests.NewSymptomRequest;
import pl.mazur.DiagnosisApp.server.services.SymptomService;

import java.util.List;

@RestController
@RequestMapping("/symptoms")
public class SymptomController {

    private final SymptomService symptomService;

    @Autowired
    public SymptomController(SymptomService symptomService) {
        this.symptomService = symptomService;
    }

    @GetMapping
    public List<SymptomEntity> getAllExistingSymptoms() {
        return symptomService.getAllExistingSymptoms();
    }

    @PostMapping
    public SymptomEntity addNewSymptom(@RequestBody NewSymptomRequest newSymptomRequest) throws ResponseStatusException {
        return symptomService.addNewSymptom(newSymptomRequest);
    }
}
