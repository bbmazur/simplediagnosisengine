package pl.mazur.DiagnosisApp.server.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.mazur.DiagnosisApp.database.entities.DiseaseEntity;
import pl.mazur.DiagnosisApp.server.requests.NewDiseaseRequest;
import pl.mazur.DiagnosisApp.server.services.DiseaseService;

import java.util.List;

@RestController
@RequestMapping("/diseases")
public class DiseaseController {

    private final DiseaseService diseaseService;

    @Autowired
    public DiseaseController(DiseaseService diseaseService) {
        this.diseaseService = diseaseService;
    }

    @GetMapping
    public List<DiseaseEntity> getAllExistingDiseases() {
        return diseaseService.getAllExistingDiseases();
    }

    @PostMapping
    public DiseaseEntity addNewDisease(@RequestBody NewDiseaseRequest newDiseaseRequest) {
        return diseaseService.addNewDisease(newDiseaseRequest);
    }
}
