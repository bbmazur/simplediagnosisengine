package pl.mazur.DiagnosisApp.server.controllers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mazur.DiagnosisApp.server.requests.CalculateProbableDiseaseRequest;
import pl.mazur.DiagnosisApp.server.services.DiagnosisService;
import pl.mazur.DiagnosisApp.server.to.DiagnosisTo;

@RestController
@RequestMapping("/diagnosis")
public class DiagnosisController {

    private final DiagnosisService diagnosisService;

    public DiagnosisController(DiagnosisService diagnosisService) {
        this.diagnosisService = diagnosisService;
    }

    @PostMapping
    public DiagnosisTo calculateMostProbableDiagnosis(@RequestBody CalculateProbableDiseaseRequest calculateProbableDiseaseRequest) {
        return diagnosisService.calculateDiagnosis(calculateProbableDiseaseRequest);
    }
}
