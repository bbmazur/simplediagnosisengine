package pl.mazur.DiagnosisApp.server.to;

import lombok.Data;
import pl.mazur.DiagnosisApp.database.entities.DiseaseEntity;

@Data
public class DiseaseTo {
    private String id;
    private String name;

    public DiseaseTo() {
    }

    public DiseaseTo(DiseaseEntity diseaseEntity) {
        this.id = diseaseEntity.getDiseaseId();
        this.name = diseaseEntity.getName();
    }
}
