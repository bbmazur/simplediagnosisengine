package pl.mazur.DiagnosisApp.server.to;

import lombok.Data;
import pl.mazur.DiagnosisApp.database.entities.DiseaseEntity;

@Data
public class DiagnosisTo {
    private DiseaseTo mostProbableDisease;

    public DiagnosisTo() {
    }

    public DiagnosisTo(DiseaseEntity diseaseEntity) {
        this.mostProbableDisease = new DiseaseTo(diseaseEntity);
    }
}
