package pl.mazur.DiagnosisApp.tools;

import pl.mazur.DiagnosisApp.database.entities.DiseaseEntity;
import pl.mazur.DiagnosisApp.database.entities.DiseaseSymptomEntity;
import pl.mazur.DiagnosisApp.database.entities.SymptomEntity;
import pl.mazur.DiagnosisApp.database.shared.Prevalence;
import pl.mazur.DiagnosisApp.database.shared.Sex;
import pl.mazur.DiagnosisApp.server.requests.CalculateProbableDiseaseRequest;
import pl.mazur.DiagnosisApp.server.requests.NewDiseaseRequest;
import pl.mazur.DiagnosisApp.server.requests.NewDiseaseSymptomRequest;
import pl.mazur.DiagnosisApp.server.requests.NewSymptomRequest;

import java.util.ArrayList;
import java.util.List;

public class ModelTool {
    public static NewSymptomRequest createSampleSymptomAMale() {
        NewSymptomRequest newSymptomRequest = new NewSymptomRequest();
        newSymptomRequest.setName("SymptomA");
        newSymptomRequest.setSex(Sex.MALE);
        return newSymptomRequest;
    }

    public static NewDiseaseRequest createSampleDiseaseARequest() {
        NewDiseaseRequest newDiseaseRequest = new NewDiseaseRequest();
        newDiseaseRequest.setName("DiseaseA");
        newDiseaseRequest.setPrevalence(Prevalence.RARE);
        newDiseaseRequest.setSex(Sex.MALE);
        List<NewDiseaseSymptomRequest> occurredSymptoms = new ArrayList<>();
        occurredSymptoms.add(ModelTool.createSampleDiseaseSymptomRequest());
        newDiseaseRequest.setOccurringSymptoms(occurredSymptoms);
        return newDiseaseRequest;
    }

    public static NewDiseaseSymptomRequest createSampleDiseaseSymptomRequest() {
        NewDiseaseSymptomRequest newDiseaseSymptomRequest = new NewDiseaseSymptomRequest();
        newDiseaseSymptomRequest.setProbability(0.01);
        newDiseaseSymptomRequest.setSymptomId("undefined");
        return newDiseaseSymptomRequest;
    }

    public static CalculateProbableDiseaseRequest createSampleCalculationRequest() {
        CalculateProbableDiseaseRequest calculateProbableDiseaseRequest = new CalculateProbableDiseaseRequest();
        calculateProbableDiseaseRequest.setSex(Sex.MALE);
        return calculateProbableDiseaseRequest;
    }

    public static List<DiseaseEntity> createSample2DiseaseEntitiesWithTwoSampleSymptoms() {
        DiseaseEntity diseaseEntityA = new DiseaseEntity();
        diseaseEntityA.setPrevalence(Prevalence.RARE);
        diseaseEntityA.setName("A");
        DiseaseEntity diseaseEntityB = new DiseaseEntity();
        diseaseEntityB.setPrevalence(Prevalence.LIKELY);
        diseaseEntityB.setName("B");
        DiseaseSymptomEntity diseaseSymptomEntityA = createSampleDiseaseSymptomEntity();
        diseaseSymptomEntityA.setSymptomId("B");
        diseaseSymptomEntityA.setProbability(0.001);
        DiseaseSymptomEntity diseaseSymptomEntityB = createSampleDiseaseSymptomEntity();
        diseaseSymptomEntityB.setSymptomId("B");
        diseaseSymptomEntityB.setProbability(0.001);
        diseaseEntityA.setOccurringSymptoms(List.of(createSampleDiseaseSymptomEntity(), diseaseSymptomEntityA));
        diseaseEntityB.setOccurringSymptoms(List.of(createSampleDiseaseSymptomEntity(), diseaseSymptomEntityB));
        return new ArrayList<>(List.of(diseaseEntityA, diseaseEntityB));
    }

    public static DiseaseSymptomEntity createSampleDiseaseSymptomEntity() {
        DiseaseSymptomEntity diseaseSymptomEntity = new DiseaseSymptomEntity();
        diseaseSymptomEntity.setSymptomId("A");
        diseaseSymptomEntity.setProbability(0.5);
        return diseaseSymptomEntity;
    }
}
