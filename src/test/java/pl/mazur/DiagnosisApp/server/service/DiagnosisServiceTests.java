package pl.mazur.DiagnosisApp.server.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.util.Pair;
import pl.mazur.DiagnosisApp.database.entities.DiseaseEntity;
import pl.mazur.DiagnosisApp.database.entities.DiseaseSymptomEntity;
import pl.mazur.DiagnosisApp.database.repositories.DiseaseRepository;
import pl.mazur.DiagnosisApp.database.shared.Sex;
import pl.mazur.DiagnosisApp.server.services.DiagnosisService;
import pl.mazur.DiagnosisApp.server.services.impl.DiagnosisServiceImpl;
import pl.mazur.DiagnosisApp.tools.ModelTool;

import java.util.List;
import java.util.Set;

@SpringBootTest
public class DiagnosisServiceTests {

    private DiagnosisService diagnosisService;

    @Autowired
    private DiseaseRepository diseaseRepository;

    @BeforeEach
    public void setup() {
        diagnosisService = new DiagnosisServiceImpl(diseaseRepository, null);
    }

    @Test
    public void getAllRelevantDiseasesIgnoresMale() {
        DiseaseEntity entityMale = new DiseaseEntity();
        entityMale.setSex(Sex.MALE);
        diseaseRepository.save(entityMale);
        DiseaseEntity entityFemale = new DiseaseEntity();
        entityFemale.setSex(Sex.FEMALE);
        diseaseRepository.save(entityFemale);
        DiseaseEntity entityBoth = new DiseaseEntity();
        entityBoth.setSex(Sex.BOTH);
        diseaseRepository.save(entityBoth);
        List<DiseaseEntity> result = diagnosisService.getAllRelevantDiseases(Sex.FEMALE);
        Assertions.assertEquals(2, result.size());
        result.forEach(dE -> {
            Assertions.assertNotEquals(Sex.MALE, dE.getSex());
        });
    }

    @Test
    public void getAllRelevantSymptomsIgnoresSymptomA() {
        Set<String> reportedSymptoms = Set.of("B");
        List<Pair<DiseaseEntity, List<DiseaseSymptomEntity>>> result = diagnosisService.getAllRelevantSymptoms(
                ModelTool.createSample2DiseaseEntitiesWithTwoSampleSymptoms(),
                reportedSymptoms);
        Assertions.assertEquals(2, result.size());
        result.forEach(p -> {
            Assertions.assertEquals(1, p.getSecond().size());
            Assertions.assertEquals("B", p.getSecond().get(0).getSymptomId());
        });
    }

    @Test
    public void getAllRelevantSymptomsTwoSymptomsEachInDifferentDisease() {
        Set<String> reportedSymptoms = Set.of("A", "B");
        List<DiseaseEntity> diseases = ModelTool.createSample2DiseaseEntitiesWithTwoSampleSymptoms();
        diseases.get(0).getOccurringSymptoms().get(0).setSymptomId("C");
        diseases.get(1).getOccurringSymptoms().get(1).setSymptomId("C");
        List<Pair<DiseaseEntity, List<DiseaseSymptomEntity>>> result = diagnosisService.getAllRelevantSymptoms(
                diseases,
                reportedSymptoms);
        Assertions.assertEquals(2, result.size());
        result.forEach(p -> {
            Assertions.assertEquals(1, p.getSecond().size());
            Assertions.assertNotEquals("C", p.getSecond().get(0).getSymptomId());
            Assertions.assertEquals(true,
                    p.getSecond().get(0).getSymptomId().equals("A") || p.getSecond().get(0).getSymptomId().equals("B"));
        });
    }

    @Test
    public void calculateMostProbableDiseaseSameSymptomsDifferentPrevalence() {
        Set<String> reportedSymptoms = Set.of("A", "B");
        List<Pair<DiseaseEntity, List<DiseaseSymptomEntity>>> relevanceResult = diagnosisService.getAllRelevantSymptoms(
                ModelTool.createSample2DiseaseEntitiesWithTwoSampleSymptoms(),
                reportedSymptoms);
        Assertions.assertEquals(2, relevanceResult.size());
        Pair<DiseaseEntity, Double> mostProbablyDisease = diagnosisService.calculateMostProbableDisease(relevanceResult);
        Assertions.assertEquals("B", mostProbablyDisease.getFirst().getName());
    }
}
