package pl.mazur.DiagnosisApp.server.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import pl.mazur.DiagnosisApp.database.entities.SymptomEntity;
import pl.mazur.DiagnosisApp.database.repositories.SymptomRepository;
import pl.mazur.DiagnosisApp.database.shared.Sex;
import pl.mazur.DiagnosisApp.server.requests.NewSymptomRequest;
import pl.mazur.DiagnosisApp.tools.ModelTool;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SymptomControllerTests {

    @LocalServerPort
    private int port;

    @Autowired
    private SymptomRepository symptomRepository;

    @Autowired
    private TestRestTemplate restTemplate;

    private String url;

    @BeforeEach
    public void setupUrls() {
        url = "http://localhost:" + port + "/symptoms";
    }

    @BeforeEach
    public void initDatabase() {
        symptomRepository.save(new SymptomEntity("SymptomA", Sex.MALE));
        symptomRepository.save(new SymptomEntity("SymptomB", Sex.FEMALE));
    }

    @AfterEach
    public void clearDb() {
        symptomRepository.deleteAll();
    }

    @Test
    public void getAllExistingSymptomsShouldReturnTwoInitializedTest() {
        SymptomEntity[] response = this.restTemplate.getForObject(this.url, SymptomEntity[].class);
        Assertions.assertEquals(2, response.length);
        Assertions.assertEquals("SymptomA", response[0].getName());
        Assertions.assertEquals("SymptomB", response[1].getName());
    }

    @Test
    public void addNewSymptomReturnsCorrectValue() {
        NewSymptomRequest newSymptomRequest = ModelTool.createSampleSymptomAMale();
        newSymptomRequest.setName("SymptomC");
        SymptomEntity response = postForSymptomEntity(newSymptomRequest);
        Assertions.assertEquals("SymptomC", response.getName());
        Assertions.assertEquals(Sex.MALE, response.getSex());
    }

    @Test
    public void addExistingSymptomThrowsException() throws JsonProcessingException {
        NewSymptomRequest newSymptomRequest = ModelTool.createSampleSymptomAMale();
        ResponseEntity<String> response = this.restTemplate.postForEntity(this.url, newSymptomRequest, String.class);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        Assertions.assertEquals("Same symptom already found!", new ObjectMapper().readValue(response.getBody(), ObjectNode.class).get("message").asText());
    }

    @Test
    public void addUnrecognizedSexThrowsException() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>("{\"name\": \"SymptomD\", \"sex\": \"unknown\"}", headers);
        ResponseEntity<String> response = this.restTemplate.postForEntity(this.url, request, String.class);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testChangeOfSymptomSex() {
        NewSymptomRequest newSymptomRequest = ModelTool.createSampleSymptomAMale();
        newSymptomRequest.setName("SymptomE");
        SymptomEntity responseNew = postForSymptomEntity(newSymptomRequest);
        newSymptomRequest.setSex(Sex.FEMALE);
        SymptomEntity responseChange = postForSymptomEntity(newSymptomRequest);
        Assertions.assertEquals(responseNew.getSymptomId(), responseChange.getSymptomId());
        Assertions.assertEquals(Sex.BOTH, responseChange.getSex());
    }

    private SymptomEntity postForSymptomEntity(NewSymptomRequest newSymptomRequest) {
        return this.restTemplate.postForObject(this.url,
                newSymptomRequest, SymptomEntity.class);
    }
}
