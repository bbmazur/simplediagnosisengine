package pl.mazur.DiagnosisApp.server.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import pl.mazur.DiagnosisApp.database.entities.DiseaseEntity;
import pl.mazur.DiagnosisApp.database.entities.DiseaseSymptomEntity;
import pl.mazur.DiagnosisApp.database.entities.SymptomEntity;
import pl.mazur.DiagnosisApp.database.repositories.DiseaseRepository;
import pl.mazur.DiagnosisApp.database.repositories.SymptomRepository;
import pl.mazur.DiagnosisApp.database.shared.Prevalence;
import pl.mazur.DiagnosisApp.database.shared.Sex;
import pl.mazur.DiagnosisApp.server.requests.CalculateProbableDiseaseRequest;
import pl.mazur.DiagnosisApp.server.requests.NewDiseaseRequest;
import pl.mazur.DiagnosisApp.server.to.DiagnosisTo;
import pl.mazur.DiagnosisApp.tools.ModelTool;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DiagnosisControllerTests {
    @LocalServerPort
    private int port;

    String urlDisease;
    String urlDiagnosis;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private DiseaseRepository diseaseRepository;

    @Autowired
    private SymptomRepository symptomRepository;

    @BeforeEach
    public void setupUrls() {
        urlDisease = "http://localhost:" + port + "/diseases";
        urlDiagnosis = "http://localhost:" + port + "/diagnosis";
    }

    @BeforeEach
    public void initDatabase() {
        DiseaseEntity diseaseEntity = new DiseaseEntity();
        diseaseEntity.setName("DiseaseA");
        diseaseEntity.setPrevalence(Prevalence.RARE);
        symptomRepository.save(new SymptomEntity("SymptomA", Sex.MALE));
        DiseaseSymptomEntity diseaseSymptomEntity = new DiseaseSymptomEntity();
        diseaseSymptomEntity.setDisease(diseaseEntity);
        diseaseSymptomEntity.setProbability(0.05);
        List<DiseaseSymptomEntity> occuredSymptoms = new ArrayList<>();
        occuredSymptoms.add(diseaseSymptomEntity);
        diseaseEntity.setOccurringSymptoms(occuredSymptoms);
        diseaseRepository.save(diseaseEntity);
    }

    @AfterEach
    public void clearDb() {
        diseaseRepository.deleteAll();
        symptomRepository.deleteAll();
    }

    @Test
    public void calculateDignosisThrowsExceptionWhenBothSexInRequest() throws JsonProcessingException {
        CalculateProbableDiseaseRequest calculateProbableDiseaseRequest = new CalculateProbableDiseaseRequest();
        calculateProbableDiseaseRequest.setSex(Sex.BOTH);
        calculateProbableDiseaseRequest.setReportedSymptoms(Set.of());
        ResponseEntity<String> response = this.restTemplate.postForEntity(urlDiagnosis,
                calculateProbableDiseaseRequest, String.class);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        Assertions.assertEquals("Diagnosis request cannot contain BOTH sex!", new ObjectMapper().readValue(response.getBody(), ObjectNode.class).get("message").asText());
    }

    @Test
    public void calculateDignosisReturnsCorrectDiagnosis() {
        SymptomEntity symptomA = symptomRepository.save(new SymptomEntity("SymptomA", Sex.MALE));
        SymptomEntity symptomB = symptomRepository.save(new SymptomEntity("SymptomB", Sex.MALE));
        NewDiseaseRequest request = ModelTool.createSampleDiseaseARequest();
        request.setName("DiseaseB");
        request.setPrevalence(Prevalence.LIKELY);
        request.getOccurringSymptoms().get(0).setSymptomId(symptomA.getSymptomId());
        DiseaseEntity responseB = this.restTemplate.postForObject(urlDisease, request, DiseaseEntity.class);
        request.setName("DiseaseA");
        request.setPrevalence(Prevalence.RARE);
        request.getOccurringSymptoms().get(0).setSymptomId(symptomB.getSymptomId());
        DiseaseEntity responseA = this.restTemplate.postForObject(urlDisease, request, DiseaseEntity.class);
        CalculateProbableDiseaseRequest calculateProbableDiseaseRequest = ModelTool.createSampleCalculationRequest();
        calculateProbableDiseaseRequest.setReportedSymptoms(Set.of(symptomA.getSymptomId(), symptomB.getSymptomId()));
        DiagnosisTo response = this.restTemplate.postForObject(urlDiagnosis, calculateProbableDiseaseRequest, DiagnosisTo.class);
        Assertions.assertEquals(responseB.getDiseaseId(), response.getMostProbableDisease().getId());
        Assertions.assertEquals("DiseaseB", response.getMostProbableDisease().getName());
    }

    @Test
    public void calculateDiagnosisThrowsSymptomNotFoundException() throws JsonProcessingException {
        CalculateProbableDiseaseRequest calculateProbableDiseaseRequest = ModelTool.createSampleCalculationRequest();
        calculateProbableDiseaseRequest.setReportedSymptoms(Set.of("undefined"));
        ResponseEntity<String> response = this.restTemplate.postForEntity(urlDiagnosis, calculateProbableDiseaseRequest, String.class);
        Assertions.assertEquals("One of given symptoms was not found in database!", new ObjectMapper().readValue(response.getBody(), ObjectNode.class).get("message").asText());
    }
}
