package pl.mazur.DiagnosisApp.server.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import pl.mazur.DiagnosisApp.database.entities.DiseaseEntity;
import pl.mazur.DiagnosisApp.database.entities.DiseaseSymptomEntity;
import pl.mazur.DiagnosisApp.database.entities.SymptomEntity;
import pl.mazur.DiagnosisApp.database.repositories.DiseaseRepository;
import pl.mazur.DiagnosisApp.database.repositories.SymptomRepository;
import pl.mazur.DiagnosisApp.database.shared.Prevalence;
import pl.mazur.DiagnosisApp.database.shared.Sex;
import pl.mazur.DiagnosisApp.server.requests.NewDiseaseRequest;
import pl.mazur.DiagnosisApp.server.requests.NewDiseaseSymptomRequest;
import pl.mazur.DiagnosisApp.tools.ModelTool;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DiseaseControllerTests {
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private DiseaseRepository diseaseRepository;

    @Autowired
    private SymptomRepository symptomRepository;

    private String url;

    @BeforeEach
    public void setupUrls() {
        url = "http://localhost:" + port + "/diseases";
    }

    @BeforeEach
    public void initDatabase() {
        DiseaseEntity diseaseEntity = new DiseaseEntity();
        SymptomEntity symptomEntity = symptomRepository.save(new SymptomEntity("SymptomA", Sex.MALE));
        DiseaseSymptomEntity diseaseSymptomEntity = new DiseaseSymptomEntity();
        diseaseSymptomEntity.setDisease(diseaseEntity);
        diseaseSymptomEntity.setProbability(0.05);
        List<DiseaseSymptomEntity> occuredSymptoms = new ArrayList<>();
        occuredSymptoms.add(diseaseSymptomEntity);
        diseaseEntity.setOccurringSymptoms(occuredSymptoms);
        diseaseRepository.save(diseaseEntity);
    }

    @AfterEach
    public void clearDb() {
        diseaseRepository.deleteAll();
        symptomRepository.deleteAll();
    }

    @Test
    public void getAllExistingDiseasesReturnsCorrectObject() {
        DiseaseEntity[] response = this.restTemplate.getForObject(url, DiseaseEntity[].class);
        Assertions.assertEquals(1, response.length);
        Assertions.assertEquals(1, response[0].getOccurringSymptoms().size());
        Assertions.assertEquals(0.05, response[0].getOccurringSymptoms().get(0).getProbability());
    }

    @Test
    public void addNewDiseaseCorrectRequest() {
        SymptomEntity symptomEntity = symptomRepository.save(new SymptomEntity("SymptomZ", Sex.MALE));
        NewDiseaseRequest newDiseaseRequest = ModelTool.createSampleDiseaseARequest();
        newDiseaseRequest.getOccurringSymptoms().get(0).setSymptomId(symptomEntity.getSymptomId());
        DiseaseEntity response = this.restTemplate.postForObject(url, newDiseaseRequest, DiseaseEntity.class);
        Assertions.assertEquals("DiseaseA", response.getName());
        Assertions.assertEquals(Prevalence.RARE, response.getPrevalence());
        Assertions.assertEquals(1, response.getOccurringSymptoms().size());
        Assertions.assertEquals(symptomEntity.getSymptomId(), response.getOccurringSymptoms().get(0).getSymptomId());
    }

    @Test
    public void addNewDiseaseThrowsExceptionWhenSymptomNotFound() throws JsonProcessingException {
        SymptomEntity symptomEntity = symptomRepository.save(new SymptomEntity("SymptomZ", Sex.MALE));
        NewDiseaseSymptomRequest newDiseaseSymptomRequest1 = new NewDiseaseSymptomRequest();
        newDiseaseSymptomRequest1.setSymptomId(symptomEntity.getSymptomId());
        newDiseaseSymptomRequest1.setProbability(0.01);
        NewDiseaseRequest newDiseaseRequest = ModelTool.createSampleDiseaseARequest();
        newDiseaseRequest.getOccurringSymptoms().add(newDiseaseSymptomRequest1);
        ResponseEntity<String> response = this.restTemplate.postForEntity(url, newDiseaseRequest, String.class);
        Assertions.assertEquals("One of given symptoms was not found in database!", new ObjectMapper().readValue(response.getBody(), ObjectNode.class).get("message").asText());
    }

    @Test
    public void addNewDiseaseThrowsBadProbabilityRangeException() throws JsonProcessingException {
        SymptomEntity symptomEntity = symptomRepository.save(new SymptomEntity("SymptomZ", Sex.MALE));
        NewDiseaseRequest newDiseaseRequest = ModelTool.createSampleDiseaseARequest();
        newDiseaseRequest.getOccurringSymptoms().get(0).setSymptomId(symptomEntity.getSymptomId());
        newDiseaseRequest.getOccurringSymptoms().get(0).setProbability(0);
        ResponseEntity<String> response = this.restTemplate.postForEntity(url, newDiseaseRequest, String.class);
        Assertions.assertEquals("One of symptoms probability is not in (0, 1> range!", new ObjectMapper().readValue(response.getBody(), ObjectNode.class).get("message").asText());
        newDiseaseRequest.getOccurringSymptoms().get(0).setProbability(1.1);
        Assertions.assertEquals("One of symptoms probability is not in (0, 1> range!", new ObjectMapper().readValue(response.getBody(), ObjectNode.class).get("message").asText());
    }
}
